#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file 00-cont-after-session-unlock.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-06-11 Friday 22:41:04 (+0200)
##  --------------------------------------------------------------------------
##     @created 2021-02-22 Monday 23:27:05 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/header.sh"
##  ==========================================================================
##  }}} Sources
##
### Monitors {{{
##  ==========================================================================
if [ "${object}" = "session/${DBUS_LOGIND_SESSION_NAME}" ]; then
  case "${interface}.${signal}" in
    Session.Unlock)
      ##
      # TODO:
      #
      : playerctl play
      ##
      for name in brave chromium conky firefox firefox.real vlc wfica; do
        a pkill -CONT -x "${name}" || :
      done
      ;;
    *)
      ;;
  esac
fi
##  ==========================================================================
##  }}} Monitors
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/footer.sh"
##  ==========================================================================
##  }}} Sources
