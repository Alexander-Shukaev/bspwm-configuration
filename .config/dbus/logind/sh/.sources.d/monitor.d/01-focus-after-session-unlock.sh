#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file 01-focus-after-session-unlock.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-02-24 Wednesday 09:54:58 (+0100)
##  --------------------------------------------------------------------------
##     @created 2021-02-22 Monday 23:27:05 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/header.sh"
##  ==========================================================================
##  }}} Sources
##
### Monitors {{{
##  ==========================================================================
if [ "${object}" = "session/${DBUS_LOGIND_SESSION_NAME}" ]; then
  case "${interface}.${signal}" in
    Session.Unlock)
      a bspc node -f "$(bspc query -N -n focused)" || :
      ;;
    *)
      ;;
  esac
fi
##  ==========================================================================
##  }}} Monitors
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/footer.sh"
##  ==========================================================================
##  }}} Sources
