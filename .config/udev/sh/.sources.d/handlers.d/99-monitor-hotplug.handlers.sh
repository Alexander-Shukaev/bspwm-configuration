#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file 99-monitor-hotplug.handlers.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-08-06 Saturday 14:19:51 (+0200)
##  --------------------------------------------------------------------------
##     @created 2020-07-22 Wednesday 18:50:28 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/header.sh"
##  ==========================================================================
##  }}} Sources
##
### Handlers {{{
##  ==========================================================================
a display-setup || :
# ----------------------------------------------------------------------------
# TODO:
#
# Verify later if this is still necessary?
#
# CAUTION:
#
# After a real change in a display setup (e.g. monitor added and/or removed),
# it appears that at least Whisker Menu (the Xfce 4 plugin
# 'libwhiskermenu.so') gets screwed up, e.g.:
#
# 1.  switching window focus away from it does not close it anymore as it used
#     to by default (as per setting 'stay-on-focus-out');
# 2.  arrow keys no longer select items in it (essentially they have no effect
#     in general);
# 3.  both of the above examples suggest issues with keyboard focus detection
#     (similar to those of Citrix ICA Client 'wfica') as pointer (mouse) focus
#     still appears to work as expected.
#
# As a result, for now, simply restart the whole Xfce 4 Panel:
#
: a systemctl --user try-restart xfce4-panel || a xfce4-panel -r || :
# ----------------------------------------------------------------------------
# DEPRECATED:
#
: a systemctl --user try-reload-or-restart bspwm || a bspc wm -r || :
# ----------------------------------------------------------------------------
##  ==========================================================================
##  }}} Handlers
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/footer.sh"
##  ==========================================================================
##  }}} Sources
##
### References {{{
##  ==========================================================================
##  [1] file:///usr/local/bin/udev-monitor-hotplug
##  ==========================================================================
##  }}} References
