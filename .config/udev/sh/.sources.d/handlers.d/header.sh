#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file header.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-12-18 Saturday 15:43:47 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-07-22 Wednesday 18:52:36 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Traps {{{
##  ==========================================================================
a trap_or_exit -- :
##  ==========================================================================
##  }}} Traps
##
### Variables {{{
##  ==========================================================================
logprefix="UDEV Event Handler '${__REALFILE__}'"
##  ==========================================================================
##  }}} Variables
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: Begin"
##  ==========================================================================
##  }}} Messages
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##  ==========================================================================
##  }}} Variables
