#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file variables.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-04-24 Sunday 10:31:58 (+0200)
##  --------------------------------------------------------------------------
##     @created 2020-05-23 Saturday 02:04:45 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Variables {{{
##  ==========================================================================
BSPWM_CONFIG_HOME="$(xdg_config_home)/bspwm"
BSPWM_ENVIRONMENT_FILE="${BSPWM_CONFIG_HOME}/bspwm.conf"
##
  focus_follows_pointer="${BSPWM_FOCUS_FOLLOWS_POINTER-}"
  pointer_follows_focus="${BSPWM_POINTER_FOLLOWS_FOCUS-}"
pointer_follows_monitor="${BSPWM_POINTER_FOLLOWS_MONITOR-}"
    screen_monitor_name="${BSPWM_SCREEN_MONITOR_NAME-}"
   subscribe_node_state="${BSPWM_SUBSCRIBE_NODE_STATE-}"
                verbose="${BSPWM_VERBOSE-}"
##
${BSPWM_FOCUS_FOLLOWS_POINTER+:}   unset -v focus_follows_pointer
${BSPWM_POINTER_FOLLOWS_FOCUS+:}   unset -v pointer_follows_focus
${BSPWM_POINTER_FOLLOWS_MONITOR+:} unset -v pointer_follows_monitor
${BSPWM_SCREEN_MONITOR_NAME+:}     unset -v screen_monitor_name
${BSPWM_SUBSCRIBE_NODE_STATE+:}    unset -v subscribe_node_state
${BSPWM_VERBOSE+:}                 unset -v verbose
##  ==========================================================================
##  }}} Variables
##
### Sources {{{
##  ==========================================================================
[ -r "${BSPWM_ENVIRONMENT_FILE}" ] && . "${BSPWM_ENVIRONMENT_FILE}" || :
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
  ${focus_follows_pointer+:} eval -- \
    focus_follows_pointer="${BSPWM_FOCUS_FOLLOWS_POINTER-true}"
  ${pointer_follows_focus+:} eval -- \
    pointer_follows_focus="${BSPWM_POINTER_FOLLOWS_FOCUS-false}"
${pointer_follows_monitor+:} eval -- \
  pointer_follows_monitor="${BSPWM_POINTER_FOLLOWS_MONITOR-true}"
    ${screen_monitor_name+:} eval -- \
      screen_monitor_name="${BSPWM_SCREEN_MONITOR_NAME-Screen}"
   ${subscribe_node_state+:} eval -- \
     subscribe_node_state="${BSPWM_SUBSCRIBE_NODE_STATE-false}"
                ${verbose+:} eval -- \
                  verbose="${BSPWM_VERBOSE-false}"
##
BSPWM_FOCUS_FOLLOWS_POINTER="${focus_follows_pointer}"
BSPWM_POINTER_FOLLOWS_FOCUS="${pointer_follows_focus}"
BSPWM_POINTER_FOLLOWS_MONITOR="${pointer_follows_monitor}"
BSPWM_SCREEN_MONITOR_NAME="${screen_monitor_name}"
BSPWM_SUBSCRIBE_NODE_STATE="${subscribe_node_state}"
BSPWM_VERBOSE="${verbose}"
##  ==========================================================================
##  }}} Variables
##
### Exports {{{
##  ==========================================================================
export BSPWM_CONFIG_HOME
export BSPWM_ENVIRONMENT_FILE
export BSPWM_FOCUS_FOLLOWS_POINTER
export BSPWM_POINTER_FOLLOWS_FOCUS
export BSPWM_POINTER_FOLLOWS_MONITOR
export BSPWM_SCREEN_MONITOR_NAME
export BSPWM_SUBSCRIBE_NODE_STATE
export BSPWM_VERBOSE
##  ==========================================================================
##  }}} Exports
