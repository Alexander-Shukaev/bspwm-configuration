#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file external-rules.sh
##  --------------------------------------------------------------------------
##     @version 0.9.10
##  --------------------------------------------------------------------------
##     @updated 2022-09-19 Monday 21:52:28 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-03-26 Saturday 20:56:54 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh" &&                    \
  . "${__REALFILEDIR__%/}/commands.sh"                 &&                    \
  . "${__REALFILEDIR__%/}/variables.sh"
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##
         wid="${1}"
       class="${2}"
    instance="${3}"
consequences="${4}"
##
primary_monitor_sel="$(q monitor_id 'primary' && echo 'primary' || echo '^1')"
##  ==========================================================================
##  }}} Variables
##
### External Rules {{{
##  ==========================================================================
case "${class}" in
  android-file-transfer)
    title="$(xtitle "${wid}")"
    case "${title}" in
      Download*Progress)
        echo 'center=on' 'focus=off' 'locked=on' 'state=floating'
        ;;
    esac
    unset -v title
    ;;
  Bloomberg)
    eval -- "${consequences}"
    if [ "${layer}" = above ]; then
      echo 'focus=off' 'sticky=on' 'state=floating'
    fi
    ;;
  Conky)
    for _wid in $(root_wids || :); do
      q above -t "${_wid}" "${wid}" || :
    done
    for _wid in $(wids -N Xfdesktop || :); do
      q above -t "${_wid}" "${wid}" || :
    done
    unset -v _wid
    echo 'manage=off'
    ;;
  Firefox)
    case "$(prop -id "${wid}" -notype _NET_WM_WINDOW_TYPE)" in
      *=*_NET_WM_WINDOW_TYPE_DIALOG)
        echo 'focus=on' 'locked=off'
        ;;
      *)
        echo 'desktop=^2' 'follow=on' 'locked=on'
        ;;
    esac
    ;;
  Gthumb)
    case "$(prop -id "${wid}" -notype _NET_WM_WINDOW_TYPE)" in
      *=*_NET_WM_WINDOW_TYPE_DIALOG)
        echo 'focus=on' 'locked=off'
        ;;
      *)
        echo 'locked=on'
        ;;
    esac
    ;;
  Launchpad*)
    monitor_sel="${primary_monitor_sel}"
    echo "desktop=${monitor_sel}:^4" 'follow=on' 'locked=on' 'state=floating'
    unset -v monitor_sel
    ;;
  Thunderbird)
    title="$(xtitle "${wid}")"
    case "${title}" in
      Password*)
        echo 'center=on' 'focus=off' 'locked=off' 'sticky=on' 'state=floating'
        ;;
      *)
        case "$(prop -id "${wid}" -notype _NET_WM_WINDOW_TYPE)" in
          *=*_NET_WM_WINDOW_TYPE_DIALOG)
            echo 'focus=on' 'locked=off'
            ;;
          *)
            echo 'locked=on'
            ;;
        esac
        ;;
    esac
    unset -v title
    ;;
  Tor*)
    title="$(xtitle "${wid}")"
    case "${title}" in
      NoScript*)
        echo 'center=on' 'focus=on' 'sticky=on' 'state=floating'
        ;;
    esac
    unset -v title
    ;;
  Wfica)
    title="$(xtitle "${wid}")"
    case "${title}" in
      '')
        echo 'locked=off' 'sticky=on' 'state=floating'
        ;;
    esac
    unset -v title
    ;;
  Wfica_Splash)
    title="$(xtitle "${wid}")"
    case "${title}" in
      'Citrix Workspace')
        ;;
      *)
        monitor_sel="${BSPWM_SCREEN_MONITOR_NAME:-${primary_monitor_sel}}"
        echo "monitor=${monitor_sel}" 'follow=on' 'locked=on' 'state=floating'
        ;;
    esac
    unset -v monitor_sel
    unset -v title
    ;;
  Xfce4-panel)
    for _wid in $(root_wids || :); do
      q above -t "${_wid}" "${wid}" || :
    done
    for _wid in $(wids -N Xfdesktop || :); do
      q above -t "${_wid}" "${wid}" || :
    done
    unset -v _wid
    ;;
  Xfdesktop)
    for _wid in $(root_wids || :); do
      q above -t "${_wid}" "${wid}" || :
    done
    unset -v _wid
    ;;
  *)
    ;;
esac
##
case "${instance}" in
  fontforge)
    title="$(xtitle "${wid}")"
    case "${title}" in
      Layers|Tools|Warning)
        echo 'focus=off'
        ;;
    esac
    unset -v title
    ;;
  xsane)
    title="$(xtitle "${wid}")"
    case "${title}" in
      Warning)
        echo 'center=on' 'state=floating'
        ;;
    esac
    unset -v title
    ;;
  *)
    ;;
esac
##
if [ -z "${class}" ] && [ -z "${instance}" ]; then
  title="$(xtitle "${wid}")"
  case "${title}" in
    'Citrix fullscreen probe window')
      monitor_sel="${BSPWM_SCREEN_MONITOR_NAME:-${primary_monitor_sel}}"
      echo "monitor=${monitor_sel}" 'locked=on' 'state=fullscreen'
      ;;
    'Citrix maximized probe window')
      monitor_sel="${primary_monitor_sel}"
      echo "monitor=${monitor_sel}" 'locked=on' 'state=floating'
      ;;
    'Page Unresponsive')
      echo 'center=on' 'state=floating'
      ;;
    *)
      ;;
  esac
  unset -v monitor_sel
  unset -v title
else
  title="$(xtitle "${wid}")"
  case "${title}" in
    *WhatsApp*)
      echo 'desktop=^3' 'follow=on' 'locked=on'
      ;;
    Whisker*Menu)
      echo 'focus=on' 'state=fullscreen'
      ;;
    *YouTube*)
      monitor_sel="${primary_monitor_sel}"
      echo "desktop=${monitor_sel}:^5" 'follow=on' 'locked=on'
      ;;
    *)
      ;;
  esac
  unset -v monitor_sel
  unset -v title
fi
##  ==========================================================================
##  }}} External Rules
