#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file commands.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-08-10 Monday 00:40:47 (+0200)
##  --------------------------------------------------------------------------
##     @created 2020-05-23 Saturday 02:04:45 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Functions {{{
##  ==========================================================================
for_each_line() {
  if [ "${#}" -gt 1 ]; then
    # ------------------------------------------------------------------------
    # TODO:
    #
    # As per [1], but requires taking care of job control:
    #
    a tail -f -n -1 "${2}"
    # ------------------------------------------------------------------------
  else
    a cat
  fi | while IFS= read -r __string__; do
    "${1}" "${__string__}"
  done
}
##  ==========================================================================
##  }}} Functions
##
### References {{{
##  ==========================================================================
# [1] http://unix.stackexchange.com/a/236528
##  ==========================================================================
##  }}} References
