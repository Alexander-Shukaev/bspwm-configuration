#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file fullscreen-after-focus.s.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-02-01 Tuesday 18:52:47 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-07-22 Wednesday 18:52:36 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/header.sh"
##  ==========================================================================
##  }}} Sources
##
### Functions {{{
##  ==========================================================================
handle() {
  IFS=' ' read -r event monitor_id desktop_id node_id <<- __END__
${1}
__END__
  if [ "${event}" = node_focus ]; then
    if [   "$(n2 wids -N Vmplayer "${node_id}")" = "${node_id}" ] ||
         [ "$(n2 wids -N Wfica    "${node_id}")" = "${node_id}" ]; then
      if ! node_match "${node_id}.fullscreen"; then
        node "${node_id}" -t '~fullscreen' || :
      fi
    fi
    if node_match "${node_id}" -m "${screen_monitor_name}"; then
      node "${node_id}" -l normal || :
    fi
  fi
}
##  ==========================================================================
##  }}} Functions
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/footer.sh"
##  ==========================================================================
##  }}} Sources
