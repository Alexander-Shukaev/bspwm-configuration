### Preamble {{{
##  ==========================================================================
##        @file bloomberg.r.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-08-31 Monday 02:06:02 (+0200)
##  --------------------------------------------------------------------------
##     @created 2020-04-03 Friday 12:14:47 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/header.sh"
##  ==========================================================================
##  }}} Sources
##
### Rules {{{
##  ==========================================================================
rule -a '*:1-BLOOMBERG'      desktop="${primary_monitor_sel}:^4" follow=on locked=on state=tiled
rule -a '*:2-BLOOMBERG'      desktop="${primary_monitor_sel}:^4" follow=on locked=on state=tiled
rule -a '*:3-BLOOMBERG'      desktop="${primary_monitor_sel}:^4" follow=on locked=on state=tiled
rule -a '*:4-BLOOMBERG'      desktop="${primary_monitor_sel}:^4" follow=on locked=on state=tiled
##
rule -a '*:Launchpad - ASH'  desktop="${primary_monitor_sel}:^4" follow=on locked=on state=floating
rule -a '*:Launchpad'        desktop="${primary_monitor_sel}:^4" follow=on locked=on state=floating
rule -a 'Launchpad'          desktop="${primary_monitor_sel}:^4" follow=on locked=on state=floating
##
rule -a 'Bloomberg Anywhere' desktop="${primary_monitor_sel}:^4" follow=on locked=on state=floating
rule -a 'Bloomberg'          desktop="${primary_monitor_sel}:^4" follow=on locked=on state=floating
rule -a 'IB Manager'         desktop="${primary_monitor_sel}:^4" follow=on locked=on state=tiled
rule -a 'MSG Message: Inbox' desktop="${primary_monitor_sel}:^4" follow=on locked=on state=tiled
rule -a 'New Tab'            desktop="${primary_monitor_sel}:^4" follow=on locked=on state=tiled
##
rule -a 'Appointment Reminders'          center=on focus=on  sticky=on state=floating
rule -a 'Click to Dial'                  center=on focus=on  sticky=on state=floating
rule -a 'Exit Bloomberg'                 center=on focus=on  sticky=on state=floating
rule -a 'IB (1)'                         center=on focus=on  sticky=on state=floating
rule -a 'IB (2)'                         center=on focus=on  sticky=on state=floating
rule -a 'IB (3)'                         center=on focus=on  sticky=on state=floating
rule -a 'IB (4)'                         center=on focus=on  sticky=on state=floating
rule -a 'Instant Bloomberg Fading Toast'           focus=off sticky=on state=floating
##  ==========================================================================
##  }}} Rules
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/footer.sh"
##  ==========================================================================
##  }}} Sources
