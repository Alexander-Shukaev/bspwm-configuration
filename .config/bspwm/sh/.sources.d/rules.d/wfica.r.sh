#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file wfica.r.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-03-27 Saturday 13:01:45 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-03-26 Saturday 21:26:51 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/header.sh"
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
monitor_sel="${BSPWM_SCREEN_MONITOR_NAME:-${primary_monitor_sel}}"
monitor_sel="${BSPWM_SCREEN_MONITOR_ID:-${monitor_sel}}"
##  ==========================================================================
##  }}} Variables
##
### Rules {{{
##  ==========================================================================
# ----------------------------------------------------------------------------
# BUG:
#
# The following rule for the window class 'Wfica_Splash' (the corresponding
# window title turns out to be "Citrix Workspace") somehow always gets
# triggered (which is especially annoying upon reloading of BSPWM
# configuration).  As a result, this rule was converted to an external rule
# (i.e. to only be triggered once when the corresponding window is actually
# created):
#
: rule -a 'Wfica_Splash' monitor="${monitor_sel}" follow=on locked=on state=floating
# ----------------------------------------------------------------------------
rule -a 'Wfica'        monitor="${monitor_sel}" follow=on locked=on state=fullscreen
rule -a 'wfica'        monitor="${monitor_sel}" follow=on locked=on state=fullscreen
rule -a '*:Wfica'      monitor="${monitor_sel}" follow=on locked=on state=fullscreen
rule -a '*:wfica'      monitor="${monitor_sel}" follow=on locked=on state=fullscreen
##
rule -a 'Wfica_ErrorOrInfo' center=on focus=on sticky=on state=floating
##  ==========================================================================
##  }}} Rules
##
### Variables {{{
##  ==========================================================================
unset -v monitor_sel
##  ==========================================================================
##  }}} Variables
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/footer.sh"
##  ==========================================================================
##  }}} Sources
