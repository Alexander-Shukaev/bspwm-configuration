#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file tradingview.r.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-01-31 Monday 10:25:01 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-03-26 Saturday 21:35:22 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/header.sh"
##  ==========================================================================
##  }}} Sources
##
### Rules {{{
##  ==========================================================================
rule -a 'TradingView' desktop='^5' follow=on locked=on
rule -a 'tradingview' desktop='^5' follow=on locked=on
##  ==========================================================================
##  }}} Rules
##
### Sources {{{
##  ==========================================================================
. "${__REALFILEDIR__%/}/footer.sh"
##  ==========================================================================
##  }}} Sources
