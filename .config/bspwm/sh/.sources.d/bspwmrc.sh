#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file bspwmrc.sh
##  --------------------------------------------------------------------------
##     @version 0.9.10
##  --------------------------------------------------------------------------
##     @updated 2021-12-18 Saturday 15:40:20 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-05-23 Saturday 02:04:45 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh" &&                    \
  . "${__REALFILEDIR__%/}/commands.sh"                 &&                    \
  . "${__REALFILEDIR__%/}/variables.sh"
##  ==========================================================================
##  }}} Sources
##
### Locks {{{
##  ==========================================================================
a lock -c -a -e -p "${0}"               -w 11 -v -- "${0}" "${@}"
a lock -c -a -s -p "$(displaylockfile)" -w 11 -v -- "${0}" "${@}"
##  ==========================================================================
##  }}} Locks
##
### Traps {{{
##  ==========================================================================
a trap_or_exit -- 'a systemd-notify --ready || :'
##  ==========================================================================
##  }}} Traps
##
### Variables {{{
##  ==========================================================================
logprefix="BSPWM Configuration '${__REALFILE__}'"
##  ==========================================================================
##  }}} Variables
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: Begin"
##  ==========================================================================
##  }}} Messages
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##
run_level="${1:-0}"
##
screen_size="$(screen_size || :)"
screen_rect="${screen_size:+${screen_size}+0+0}"
##
screen_monitor_id=
screen_monitor_rect="${screen_rect}"
##
primary_monitor_sel="$(q monitor_id 'primary' && echo 'primary' || echo '^1')"
##  ==========================================================================
##  }}} Variables
##
### Geometry {{{
##  ==========================================================================
config border_width 1
config window_gap   0
##  ==========================================================================
##  }}} Geometry
##
### Splitting {{{
##  ==========================================================================
# ----------------------------------------------------------------------------
# DEPRECATED:
#
: config auto_alternate true
: config auto_cancel    true
# ----------------------------------------------------------------------------
##
config automatic_scheme            spiral
config directional_focus_tightness high
config initial_polarity            first_child
config removal_adjustment          true
config split_ratio                 0.5
##  ==========================================================================
##  }}} Splitting
##
### Extended Window Manager Hints (EWMH) {{{
##  ==========================================================================
config ignore_ewmh_focus      true
config ignore_ewmh_fullscreen none
config ignore_ewmh_struts     false
##  ==========================================================================
##  }}} Extended Window Manager Hints (EWMH)
##
### Focus {{{
##  ==========================================================================
# TODO:
#
config focus_follows_pointer   "${focus_follows_pointer:-false}"   || :
config pointer_follows_focus   "${pointer_follows_focus:-false}"   || :
config pointer_follows_monitor "${pointer_follows_monitor:-false}" || :
##
: config click_to_focus      button1
: config swallow_first_click true
##  ==========================================================================
##  }}} Focus
##
### Pointer {{{
##  ==========================================================================
config pointer_modifier mod1
config pointer_action1  move
config pointer_action2  resize_side
config pointer_action3  resize_corner
##  ==========================================================================
##  }}} Pointer
##
### Monocle {{{
##  ==========================================================================
# ----------------------------------------------------------------------------
# DEPRECATED:
#
: config paddingless_monocle false
# ----------------------------------------------------------------------------
##
config borderless_monocle true
config    gapless_monocle true
config     single_monocle true
##  ==========================================================================
##  }}} Monocle
##
### Miscellaneous {{{
##  ==========================================================================
# DEPRECATED:
#
: config adaptative_raise    true
: config apply_floating_atom true
##  ==========================================================================
##  }}} Miscellaneous
##
### Colors {{{
##  ==========================================================================
config  active_border_color '#FFFF00'
config focused_border_color '#00FFFF'
##
# TODO:
#
: config normal_border_color
: config presel_feedback_color
##  ==========================================================================
##  }}} Colors
##
### Opacities {{{
##  ==========================================================================
# TODO:
#
: config focused_frame_opacity 0.05
: config  normal_frame_opacity 1.00
##  ==========================================================================
##  }}} Opacities
##
### Monitors {{{
##  ==========================================================================
# ----------------------------------------------------------------------------
# TODO:
#
# Unfortunately, the topic of dynamically adding and/or removing monitors is
# rather complex.  It is true that the ideal solution is to have two following
# configuration settings set to 'true' and handle it properly with the
# corresponding 'monitor_add' and 'monitor_remove' events.  Namely,
#
# 1.  Padding should be readjusted (e.g. to re-accommodate panels properly
#     after their potential migration to a new primary monitor).
# 2.  Desktops should be rearranged in a smart way (see the "Desktops" section
#     below).
# 3.  Geometry of nodes should be readjusted according to the sizes of the
#     corresponding monitors if some of their dimensions don't fit into those
#     of the monitors (can be easily demonstrated with custom spanning monitor
#     (e.g. "Screen") even without 'remove_unplugged_monitors' being set to
#     'true', simply unplug one of the monitors and notice how the geometry of
#     the custom spanning monitor shrinks correctly, but nodes contained in it
#     remain with larger dimensions).  In fact, is this a bug or a feature of
#     BSPWM?
# ----------------------------------------------------------------------------
# CAUTION:
#
# The two following configuration settings must be set to 'true' before any
# other monitor and/or desktop manipulations take place:
#
: config remove_disabled_monitors  true
# ----------------------------------------------------------------------------
# CAUTION:
#
# Custom spanning monitor (e.g. "Screen"), as per variables
# 'screen_monitor_name' and 'screen_monitor_id', is rightfully (though
# surprisingly) treated as "unplugged", what causes its removal (event
# 'monitor_remove') and subsequent (re)creation (event 'monitor_add').  As a
# result, nodes (windows) and desktops which belonged to that monitor are
# transferred elsewhere and removed respectively.  This behavior is annoying,
# so refrain from running the following command for the time being:
#
: config remove_unplugged_monitors true
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
# DEPRECATED:
#
: s monitor "${screen_monitor_name}" -d || :
: s monitor "${screen_monitor_name}" -r || :
# ----------------------------------------------------------------------------
##
config                                top_padding  0
config                               left_padding  0
config                              right_padding  0
config                             bottom_padding  0
config -m "${primary_monitor_sel}"    top_padding  0
config -m "${primary_monitor_sel}"   left_padding 32
config -m "${primary_monitor_sel}"  right_padding  0
config -m "${primary_monitor_sel}" bottom_padding 32
##
if [ -n "${screen_monitor_name}" ] && [ -n "${screen_monitor_rect}" ]; then
  s monitor "${screen_monitor_name}" -g "${screen_monitor_rect}" ||          \
    wm -a   "${screen_monitor_name}"    "${screen_monitor_rect}" || :
  if screen_monitor_id="$(monitor_id "${screen_monitor_name}")"; then
    echo "Added screen monitor '${screen_monitor_name}'"                     \
         "with ID '${screen_monitor_id}'"                                    \
         "and rectangle '${screen_monitor_rect}'"
    wm -O $(monitor_names_append "${screen_monitor_name}" || :) || :
  else
    screen_monitor_id=
  fi
fi
##
monitor -f "${primary_monitor_sel}"
##  ==========================================================================
##  }}} Monitors
##
### Desktops {{{
##  ==========================================================================
# TODO:
#
# 1.  Loop through all desktops and those which belong to another monitor than
#     the one they are attached to, transfer them to that other monitor if it
#     exists.
# 2.  Only reset desktops per monitor if their number is less than 5.
# 3.  Do the same per 'monitor_add' event.
#
if [ "${desktop_name_uses_monitor_id:-false}" != false ]; then
  if [ -n "${screen_monitor_id}" ]; then
    monitor_ids="$(monitor_ids_sans "${screen_monitor_id}")"
  else
    monitor_ids="$(monitor_ids)"
  fi
  for monitor_id in ${monitor_ids}; do
    monitor "${monitor_id}" -d $(prefix "${monitor_id}/" 1 2 3 4 5)
  done
  if [ -n "${screen_monitor_id}" ]; then
    monitor_id="${screen_monitor_id}"
    monitor "${monitor_id}" -d "${monitor_id}/1"
  fi
  monitor_id="$(monitor_id)"
  desktop_name="${monitor_id}/1"
  a   [ "${monitor_id}" = "$(monitor_id "${primary_monitor_sel}")" ] &&
    a [ "${monitor_id}" = "$(monitor_id -d "${desktop_name}")"     ] &&
    desktop -f "${desktop_name}" || monitor -f "${primary_monitor_sel}"
  unset -v monitor_id
  unset -v monitor_ids
else
  if [ -n "${screen_monitor_name}" ]; then
    monitor_names="$(monitor_names_sans "${screen_monitor_name}")"
  else
    monitor_names="$(monitor_names)"
  fi
  for monitor_name in ${monitor_names}; do
    monitor "${monitor_name}" -d $(prefix "${monitor_name}/" 1 2 3 4 5)
  done
  if [ -n "${screen_monitor_name}" ]; then
    monitor_name="${screen_monitor_name}"
    monitor "${monitor_name}" -d "${monitor_name}/1"
  fi
  monitor_name="$(monitor_name)"
  desktop_name="${monitor_name}/1"
  a   [ "${monitor_name}" = "$(monitor_name "${primary_monitor_sel}")" ] &&
    a [ "${monitor_name}" = "$(monitor_name -d "${desktop_name}")"     ] &&
    desktop -f "${desktop_name}" || monitor -f "${primary_monitor_sel}"
  unset -v monitor_name
  unset -v monitor_names
fi
unset -v desktop_name
##  ==========================================================================
##  }}} Desktops
##
### Variables {{{
##  ==========================================================================
BSPWM_RUN_LEVEL="${run_level}"
BSPWM_SCREEN_MONITOR_ID="${screen_monitor_id}"
BSPWM_SCREEN_MONITOR_RECT="${screen_monitor_rect}"
##  ==========================================================================
##  }}} Variables
##
### Exports {{{
##  ==========================================================================
export BSPWM_RUN_LEVEL
export BSPWM_SCREEN_MONITOR_ID
export BSPWM_SCREEN_MONITOR_RECT
##  ==========================================================================
##  }}} Exports
##
### Messages {{{
##  ==========================================================================
if [ "${verbose:-false}" != false ]; then
  echo "BSPWM Environment:"
  export -p | cut -d ' ' -f 2- | grep -e '^BSPWM_'
fi
##  ==========================================================================
##  }}} Messages
##
### Rules {{{
##  ==========================================================================
for rule in "${BSPWM_CONFIG_HOME}/rules.d/"*.r; do
  a "${rule}" || :
done
unset -v rule
##
# TODO:
#
# Very intrusive with respect to tiling as per automatic scheme:
#
: rule -a '*:*' private=on
##  ==========================================================================
##  }}} Rules
##
### External Rules {{{
##  ==========================================================================
config external_rules_command "${BSPWM_CONFIG_HOME}/external-rules"
##  ==========================================================================
##  }}} External Rules
##
### Windows {{{
##  ==========================================================================
for wid in $(top_wids --onlyvisible || :); do
  if hide "${wid}" && show "${wid}"; then
    if [ "${verbose:-false}" != false ]; then
      echo "Managed window '${wid}':"
      prop -id "${wid}" -notype WM_CLASS WM_NAME || :
    fi
  fi
done
unset -v wid
##
wm --adopt-orphans
##  ==========================================================================
##  }}} Windows
##
### Panels {{{
##  ==========================================================================
for wid in $(root_wids || :); do
  above -t "${wid}" -N Conky       || :
  above -t "${wid}" -N Xfce4-panel || :
  above -t "${wid}" -N Xfdesktop   || :
done
for wid in $(wids -N Xfdesktop || :); do
  above -t "${wid}" -N Conky       || :
  above -t "${wid}" -N Xfce4-panel || :
done
unset -v wid
##
# DEPRECATED:
#
if [ "${subscribe_node_state:-false}" != false ]; then
  subscribe node_state | while read event                                    \
                                    monitor_id                               \
                                    desktop_id                               \
                                    node_id                                  \
                                    state                                    \
                                    value; do
    if [ "${state}" = fullscreen ]; then
      primary_monitor_id="$(monitor_id "${primary_monitor_sel}" || :)"
      if [ "${monitor_id}" = "${primary_monitor_id}" ] ||                    \
         [ "${monitor_id}" = "${screen_monitor_id}"  ]; then
        if [ "${value}" = on ] || [ "${value}" = true ]; then
          lower -N Xfce4-panel
        else
          raise -N Xfce4-panel
        fi
      fi
    fi
  done &
fi
##  ==========================================================================
##  }}} Panels
##
### Services {{{
##  ==========================================================================
# ----------------------------------------------------------------------------
# BUG:
#
# As per [21]:
#
: a systemctl --user try-reload-or-restart sxhkd || {
:   a pkill -USR1 -x sxhkd || :
}
# ----------------------------------------------------------------------------
##
a systemctl --user try-restart sxhkd || {
  a pkill -x sxhkd && a daemon sxhkd || :
}
##  ==========================================================================
##  }}} Services
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: End"
##  ==========================================================================
##  }}} Messages
##
### References {{{
##  ==========================================================================
#  [1] http://askubuntu.com/a/106359
#  [2] http://blog.n01se.net/blog-n01se-net-p-145.html
#  [3] http://mihids.blogspot.com/2015/02/detaching-process-from-terminal-exec.html
#  [4] http://morningcoffee.io/killing-a-process-and-all-of-its-descendants.html
#  [5] http://reddit.com/r/linux/comments/4et32j/the_basics_of_daemons
#  [6] http://stackoverflow.com/a/16317668
#  [7] http://stackoverflow.com/a/16655124
#  [8] http://stackoverflow.com/a/2614477
#  [9] http://stackoverflow.com/a/5386753
# [10] http://stackoverflow.com/a/56650580
# [11] http://stackoverflow.com/a/881434
# [12] http://stackoverflow.com/a/8915059
# [13] http://stackoverflow.com/a/958454
# [14] http://stackoverflow.com/a/9685973
# [15] http://stackoverflow.com/a/22741718
# [16] http://superuser.com/a/172476
# [17] http://unix.stackexchange.com/a/138498
# [18] http://unix.stackexchange.com/a/139230
# [19] http://unix.stackexchange.com/a/240657
# [20] http://unix.stackexchange.com/a/352923
# [21] http://github.com/baskerville/sxhkd/issues/132
##  ==========================================================================
##  }}} References
