#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file commands.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-08-06 Saturday 13:47:28 (+0200)
##  --------------------------------------------------------------------------
##     @created 2020-05-23 Saturday 02:04:45 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Functions {{{
##  ==========================================================================
config() {
  a bspc config "${@}"
}
##
desktop() {
  a bspc desktop "${@}"
}
##
monitor() {
  a bspc monitor "${@}"
}
##
monitor_id() {
  monitor_ids -m "${@}"
}
##
monitor_ids() {
  set -- "$(query_retry_timeout -M "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}"
  else
    return "${2}"
  fi
}
##
monitor_ids_sans() {
  if [ "${#}" -gt 0 ]; then
    set -- "$(monitor_ids)" "${?}" "${@}"
    if [ "${2}" -eq 0 ]; then
      echo "${1}" | {
        shift
        shift
        exclude "${@}"
      }
    else
      return "${2}"
    fi
  else
    monitor_ids
  fi
}
##
monitor_name() {
  monitor_names -m "${@}"
}
##
monitor_names() {
  set -- "$(query_retry_timeout -M --names "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}"
  else
    return "${2}"
  fi
}
##
monitor_names_sans() {
  if [ "${#}" -gt 0 ]; then
    set -- "$(monitor_names)" "${?}" "${@}"
    if [ "${2}" -eq 0 ]; then
      echo "${1}" | {
        shift
        shift
        exclude "${@}"
      }
    else
      return "${2}"
    fi
  else
    monitor_names
  fi
}
##
monitor_names_append() {
  echo $(monitor_names_sans "${@}") "${@}"
}
##
monitor_names_prepend() {
  echo "${@}" $(monitor_names_sans "${@}")
}
##
node() {
  a bspc node "${@}"
}
##
node_match() {
  [ "$(bspc query -N -n "${@}")" = "${1%%.*}" ]
}
##
query() {
  a bspc query "${@}"
}
##
query_retry_timeout() {
  retry -i 0 -t 0.1 -q -- bspc query "${@}"
}
##
rule() {
  a bspc rule "${@}"
}
##
subscribe() {
  a bspc subscribe "${@}"
}
##
wm() {
  a bspc wm "${@}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
above() {
  a xdo above "${@}"
}
##
below() {
  a xdo below "${@}"
}
##
raise() {
  a xdo raise "${@}"
}
##
lower() {
  a xdo lower "${@}"
}
##
hide() {
  a xdo hide "${@}"
}
##
show() {
  a xdo show "${@}"
}
##
wids() {
  a xdo id "${@}"
}
##
root_wids() {
  wids -N Bspwm -n root
}
##
top_wids() {
  a retry -i 0 -q -- xdotool search --all "${@}" --maxdepth 1 '' | {
    exclude $(a retry -i 0 -q -- xdotool search --all "${@}" --class Bspwm)
  }
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
prop() {
  a xprop "${@}"
}
##
prop_match() {
  grep -e "^[[:blank:]]*${1}[[:blank:]]*=[[:blank:]]*${2}" -q
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
screen_size() {
  a xdpyinfo | a awk '/dimensions:/ { print $2 }'
}
##  ==========================================================================
##  }}} Functions
